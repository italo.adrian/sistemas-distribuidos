import requests
from redis_shard.shard import RedisShardAPI
from flask import Flask, jsonify
import redis
import json
import os
import time
import threading




from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
import redis
limiter = Limiter(key_func=get_remote_address)
#ID contenedores

container1_id = "cache-redis-1"
container2_id = "cache-redis-2"
container3_id = "cache-redis-3"

def get_redis_conn(id):
    if id <= 100:
        value = redis.Redis(host='127.0.0.1', port=6379)
        return value,container1_id
    elif id <= 200:
        value = redis.Redis(host='127.0.0.1', port=6380)
        return value, container2_id
    else:
        value = redis.Redis(host='127.0.0.1', port=6381)
        return value, container3_id

def set_value(id, key, value):
    redis_conn, id_redis = get_redis_conn(id)
    redis_conn.set(key, json.dumps(value))

def get_value(id, key):
    inicio = time.time()
    redis_conn,id_redis = get_redis_conn(id)
    value = redis_conn.get(key)
    fin = time.time()
    total = (fin-inicio)*1000 #Milisegundos
    return value,total,id_redis


filename = 'json_metrics'
lock = threading.Lock()

if not os.path.exists(filename):
    with open(filename, 'w') as f:
        json.dump([], f)
counter = 0

def metrics(hostname,latency,key):
    global counter
    new_data = {'hostname':hostname,'latency':latency,'key':key, 'counter':counter}

    # Bloquear el archivo mientras se escribe para evitar problemas de concurrencia
    with lock:
        try:
            with open(filename, 'r') as f:
                data = json.load(f)
        except (json.JSONDecodeError, FileNotFoundError):
            data = []

        data.append(new_data)

        with open(filename, 'w') as f:
            json.dump(data,f,indent=3)

    counter += 1


app = Flask(__name__)


@app.route('/api/v1/hello', methods=['GET'])
def hello():
    return jsonify({'message': 'Hello World!'})

@app.route('/episodes/<id>')
def get_episodes_byId(id):
    id = int(id)
    key = f"episodes:{id}"

    episodes, total_time, redis_id = get_value(id,key)
    if episodes:
        metrics(redis_id,total_time,key)
        return jsonify(json.loads(episodes))
    else:
        url = 'https://bobsburgers-api.herokuapp.com/episodes/{id}'
        tiempo_ini = time.time()
        response = requests.get(url)
        tiempo_fin = time.time()
        latencia = (tiempo_fin-tiempo_ini)*1000
        host_name = "API"
        episodes = response.json()
        set_value(id,key,episodes)
        metrics(host_name,latencia,key)
        return (episodes)

@app.route('/characters/<id>')
def get_characters_byId(id):
    id2 = int(id)
    key = f"characters:{id}"
    characters, total_time, redis_id = get_value(id2,key)
    if characters:
        metrics(redis_id,total_time,key)
        return jsonify(json.loads(characters))

    
    url2 = 'https://bobsburgers-api.herokuapp.com/characters/{id}'
    tiempo_ini = time.time()
    response = requests.get(url2)
    tiempo_fin = time.time()
    latencia = (tiempo_fin-tiempo_ini)*1000
    host_name = "API"

    characters = response.json()
    set_value(id2,key,characters)
    metrics(host_name,latencia,key)
    return (characters)


if __name__ == '__main__':
    app.run(debug=True)