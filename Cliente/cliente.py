import requests
import time
import random


def conn():
    requests_count = 0
    close = 0

    # Tiempo máximo de ejecución de la consulta (en segundos)
    tiempo_maximo = 180

    # Realizar consultas a la API durante 60 segundos
    inicio = time.time()

    while time.time() - inicio < tiempo_maximo:
        l_random = random.randint(1,2)
        if l_random == 1: #Characters
            id = str(random.randint(1, 506))
            url = "http://host.docker.internal:5000/characters/"+id
            try:
                requests.get(url)
            except requests.exceptions.Timeout as e:
                print(f"Timeout error: {e}")
                return
            except requests.exceptions.ConnectionError as e:
                print(f"Connection error: {e}")
                return
            except requests.exceptions.RequestException as e:
                print(f"Unexpected error: {e}")
                return      
            requests_count += 1
            
        elif l_random == 2: #Episodes
            id = str(random.randint(1,238))        
            url = "http://host.docker.internal:5000/episodes/"+id
            try:
                requests.get(url)
            except requests.exceptions.Timeout as e:
                print(f"Timeout error: {e}")
                return
            except requests.exceptions.ConnectionError as e:
                print(f"Connection error: {e}")
                return
            except requests.exceptions.RequestException as e:
                print(f"Unexpected error: {e}")
                return     
            requests_count += 1
            
    print("Número total de solicitudes realizadas: ", requests_count)

conn()