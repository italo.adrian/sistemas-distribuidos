# Sistemas Distribuidos



## Comenzando

Para ejecutar este programa, debe seguir los siguientes pasos.

## Pre-requisitos

Antes de ejecutar el archivo backend.py, debe instalar las siguientes librerias.

```
pip install redis-shard
pip install Flask
pip install redis
pip install requests

```
Debe tener instalado docker y docker-compose en su maquina.

## Instalación

### Ejecutar instancias Redis

Para ejecutar las instancias redis, debe ubicarse en la carpeta **./backend/** y correr el siguiente comando.

```
docker compose up --build

```

### Correr el servidor

Para inicializar el servior, debe ubicarse en la carpeta **./backend/** y correr el siguiente comando.

```
python3 backend.py

```

### Ejecutar clientes

Para ejecutar una cantidad determinadas de cliente, debe correr el siguiente comando en la carpeta **/cliente/**

Generamos una imagen cliente

```
docker build -t cliente .

```
Ejecutamos N clientes con el siguiente comando

```
docker compose up -d --scale cliente=N

```

